user "webpon" do
  password "$1$X88arf6C$oefw24xssfldwbAw7t77j1"
  supports :manage_home => true
end

group "webpon" do
  action [:modify]
  members ["webpon"]
  append true
end