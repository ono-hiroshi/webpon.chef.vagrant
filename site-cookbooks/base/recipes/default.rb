#
# Cookbook Name:: base
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "base::import-basic-rpms"

include_recipe "base::add-yum-repositories"

include_recipe "base::add-user"

include_recipe "base::firewall"
