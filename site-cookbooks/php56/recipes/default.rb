#
# Cookbook Name:: php56
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#



# add remi-php56 repo
yum_repository 'remi-php56' do
  description 'Les RPM de Remi php56 - Repository'
  baseurl 'http://rpms.famillecollet.com/enterprise/6.6/php56/x86_64/'
  gpgkey 'http://rpms.famillecollet.com/RPM-GPG-KEY-remi'
  fastestmirror_enabled true
  action :create
end


#execute "install remirepo-key" do
#  command "rpm --import http://rpms.famillecollet.com/RPM-GPG-KEY-remi"
#end

#install php packages
%w[
  php
  php-devel
  php-opcache
  php-mbstring
  php-pdo
  php-mysqlnd
  php-pecl-xdebug
  php-pecl-jsonc
  php-pecl-jsonc-devel
  php-pecl-redis
  php-fpm
  php-mcrypt
  php-xml
].each do |pkg|
  package "#{pkg}" do
    action :install
  end
end

#change php.ini
template "php.ini" do
  path "/etc/php.ini"
  source "php.ini.erb"
  action :create
end

#change opcache.ini
template "10-opcache.ini" do
  path "/etc/php.d/10-opcache.ini"
  source "10-opcache.ini.erb"
  mode 0644
  action :create
end