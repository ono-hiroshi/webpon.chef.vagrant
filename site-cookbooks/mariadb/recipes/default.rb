#
# Cookbook Name:: mariadb

# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# add MariaDBrepo
yum_repository 'mariadb' do
  description 'MariaDB - Repository'
  baseurl 'http://yum.mariadb.org/10.1.3/centos6-x86/'
  gpgkey 'https://yum.mariadb.org/RPM-GPG-KEY-MariaDB'
  fastestmirror_enabled true
  action :create
end

package "mysql-libs" do
  action :remove
end

%w[
  MariaDB-devel
  MariaDB-client
  MariaDB-server
  galera
].each do |pkg|
  package "#{pkg}" do
    action :install
  end
end

#change server.cnf
template "server.cnf" do
  path "/etc/my.cnf.d/server.cnf"
  source "server.cnf.erb"
  action :create
end

#change mysql-clients.cnf.erb
template "mysql-clients.cnf" do
  path "/etc/my.cnf.d/mysql-clients.cnf"
  source "mysql-clients.cnf.erb"
  action :create
end

#start mysql
service "mysql" do
  supports :restart => true, :reload => true
  action :enable
end