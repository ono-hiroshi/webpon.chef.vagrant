#update all packages
execute "yum-update" do
  command "yum -y update"
  action :run
end

execute "groupinstall 'Development Tools'" do
  command "yum -y groupinstall 'Development Tools'"
  action :run
end

package "git" do
  action :install
end

package "vim" do
  action :install
end

package "mlocate" do
  action :install
end
