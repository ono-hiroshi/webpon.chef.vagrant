#
# Cookbook Name:: module
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

#.sshディレクトリ作成
directory "/home/webpon/.ssh" do
  owner "webpon"
  group "webpon"
  mode 0700
  action :create
end

#Bitbucket用秘密鍵を設置
template "/home/webpon/.ssh/id_rsa" do
  source "id_rsa.erb"
  mode 0600
  owner "webpon"
  group "webpon"
end

#key追加を聞かれないようにconfigを設置
template "/home/webpon/.ssh/config" do
  source "config.erb"
  mode 0600
  owner "webpon"
  group "webpon"
end

#git clone
git "/home/webpon/webpon" do
  repository "git@bitbucket.org:fkdevelopers/webpon.git"
  revision "master"
  user "webpon"
  group "webpon"
  #action :checkout
end

#モジュールパーミッションの変更
directory "/home/webpon/webpon/app/storage/" do
  owner "webpon"
  group "webpon"
  mode "777"
  recursive true
  action :create
end