#
# Cookbook Name:: nginx
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# add nginx-repo
yum_repository 'nginx' do
  description 'nginx - Repository'
  baseurl 'http://nginx.org/packages/centos/6/x86_64/'
  gpgkey  'http://nginx.org/packages/keys/nginx_signing.key'
  action :create
end

#install nginx
package "nginx" do
  action :install
end

#change www.conf
template "www.conf" do
  path "/etc/php-fpm.d/www.conf"
  source "www.conf.erb"
  action :create
end

#change default.conf
template "default.conf" do
  path "/etc/nginx/conf.d/default.conf"
  source "default.conf.erb"
  action :create
end

#change default.conf
template "default.conf" do
  path "/etc/nginx/conf.d/default.conf"
  source "default.conf.erb"
  action :create
end

#change /etc/nginx/conf.d/webpon.conf
template "webpon.conf" do
  path "/etc/nginx/conf.d/webpon.conf"
  source "webpon.conf.erb"
  action :create
end

#make key directory
directory "/etc/nginx/conf.d/keys" do
  owner "root"
  group "root"
  mode 0600
  action :create
end

#move server key
template "server.key" do
  path "/etc/nginx/conf.d/keys/server.key"
  source "server.key.erb"
  mode 0600
  action :create
end

#move server crt
template "server.crt" do
  path "/etc/nginx/conf.d/keys/server.crt"
  source "server.crt.erb"
  mode 0600
  action :create
end

#change /etc/nginx/conf.d/webpon_code_coverage.conf
template "webpon_code_coverage.conf" do
  path "/etc/nginx/conf.d/webpon_code_coverage.conf"
  source "webpon_code_coverage.conf.erb"
  action :create
end

#start php-fpm
service "php-fpm" do
  action :start
end

#start nginx
service "nginx" do
  action :start
end