#
# Cookbook Name:: composer
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

execute "install composer" do
    command "curl -sS https://getcomposer.org/installer | php"
    #not_if "which composer"
end

execute "move composer command" do
  command "mv composer.phar /usr/local/bin/composer"
  #not_if "which composer"
end